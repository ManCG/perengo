###Task 3: Create an automated API level test scenario
    (Expected duration: 15-30 minutes)
    
    Unfortunattely, software application are not perfect neither are the engineers that build them, so as you could see some of the API specs were not followed. To avoid having to manually run all the tests again after each new app version is to be released a good strategy is to create automated tests that could be run quickly and with little effort.
    
    We need you to:
    
    1) Build a script/program (using any language/framework/library you want) that runs tests to assert each of the API non-compliant scenarios you found; 2) Create a Readme file telling us how to run it; and 3) Commit and push everything to a folder called api-tests on the public repository you've created on Task 1.
    
    
I wrote tests written in Postman

####To install newman
follow [instructions](https://learning.getpostman.com/docs/postman/collection_runs/command_line_integration_with_newman/) 

### To run tests
$` newman run perengo.postman_collection.json`
