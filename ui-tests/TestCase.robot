*** Settings ***
Suite Setup    Open Browser    http://localhost:3000/    chrome
Suite Teardown    Close Browser
Resource    seleniumLibrary.robot


*** Test Cases ***
Test Case
    click    id=name
    type    id=name    User1
    click    xpath=(.//*[normalize-space(text()) and normalize-space(.)='Welcome to Customer App'])[1]/following::input[2]
    click    link=Americas Inc.
    click    xpath=(.//*[normalize-space(text()) and normalize-space(.)='# of Employees:'])[1]/following::p[1]
    click    xpath=(.//*[normalize-space(text()) and normalize-space(.)='# of Employees:'])[1]/following::p[1]
    doubleClick    xpath=(.//*[normalize-space(text()) and normalize-space(.)='# of Employees:'])[1]/following::p[1]
    verifyText    xpath=(.//*[normalize-space(text()) and normalize-space(.)='# of Employees:'])[1]/following::p[1]    Size: Small
    click    xpath=(.//*[normalize-space(text()) and normalize-space(.)='Name:'])[1]/following::p[1]
    click    xpath=(.//*[normalize-space(text()) and normalize-space(.)='Name:'])[1]/following::p[1]
    doubleClick    xpath=(.//*[normalize-space(text()) and normalize-space(.)='Name:'])[1]/following::p[1]
    click    xpath=(.//*[normalize-space(text()) and normalize-space(.)='Customer Details'])[1]/following::p[1]
    click    xpath=(.//*[normalize-space(text()) and normalize-space(.)='Size:'])[1]/following::p[1]
    verifyText    xpath=(.//*[normalize-space(text()) and normalize-space(.)='Size:'])[1]/following::p[1]    Contact: John Smith (jsmith@americasinc.com)
    click    xpath=(.//*[normalize-space(text()) and normalize-space(.)='Contact:'])[1]/following::input[1]
    click    link=Caribian Airlnis
    click    xpath=(.//*[normalize-space(text()) and normalize-space(.)='Size:'])[1]/following::p[1]
    verifyText    xpath=(.//*[normalize-space(text()) and normalize-space(.)='Size:'])[1]/following::p[1]    Contact: Jose Martinez (martines@cair.com)
    click    xpath=(.//*[normalize-space(text()) and normalize-space(.)='Contact:'])[1]/following::input[1]
    