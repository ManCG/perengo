* Required Chrome installes and Python 3
* Recommended virtual enviroment, eg `python3 -m venv ~/venv `
* To activate it `source ~/venv/bin/activate `
* Install dependencies: `pip install -r requirements.txt`
* To run it `python3 -m robot TestCase.robot`