# Test Plan Perengo Test
Test Plan for web application


## Document Usage Guide
 


##### Assumptions



## Testing

### Smoke Tests

From exercise 2:
     
     The Welcome Screen presents the user to a form where he/she can be identified. The form consists of:
     
     Instructions text: "Please provide your name:";
     A text field where the user will input his/her name; and
     A Submit button;
     When the user fills in his/her name and click the button, the Customer List Screen is presented. If the user clicks the button leaving the text field blank, an alert message is presented: Please provide your name.
     
     Customer List Screen
     
     This screen presents the list of all registered customers. For each customer, the following info is shown:
     
     Name
     # of Employees
     Size: if # of Employees is less than or equal 100, size is Small; if greater then 10 and less then or equal 1000, Medium; otherwise, Big
     When the user clicks on a customer name, the Contacts Detail Screen is shown.
     
     Contacts Detail Screen
     
     This screen shows the customers detailed info (Name, # of Employees, and Size) and also the name and e-mail of the person in the company to be contacted.
     
     When a customer doesn't have contact info, the message No contact info available should be presented.
     
     A Back to the list button is also presented. When it is clicked, the user is taken back to Customer List Screen.
     
#### Customer List Screen

#####Steps:
1. Open url
2. Fill input with a name
3. Click submit button
#####Expected result:
A list of all registered customers. Each costumer contains:
* Name
* Mumber of Employees
* Size of Employees: Small <=100, 100 < Medium <=1000, Large > 1000


#### Contact Info

#####Steps:
1. Open url
2. Fill input with a name
3. Click submit button
4. Click on a customer name
#####Expected result:
Shows details of costumer
* Name 
* Number of Employees
* Size
* Name and e-mail of the person in the company to be contacted.
#####Note:
If a customer doesn't have contact info, the message No contact info available should be presented.


#### Back to Customer List Screen

#####Steps:
1. Execute steps of **Contact Info**
2. Click back
#####Expected result:
You see the _Customer List Screen_

